from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from .models import Municipio, Pais, Departamento
from django.shortcuts import render
from django.db.models import Q
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login as auth_login
from django.http import HttpResponseRedirect

class lista(ListView):
    model = Municipio
    template_name = 'municipio/lista.html'
    context_object_name = 'municipios'
    paginate_by = 10  
    def get_queryset(self):
        query = self.request.GET.get('municipio')
        if query:
            return Municipio.objects.filter(Q(nombre__icontains=query)) 
        else:
            return Municipio.objects.all()


class crear(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    permission_required = 'home.add_municipio'
    model = Municipio
    template_name = 'municipio/crear.html'
    fields = ['nombre', 'departamento']
    success_url = reverse_lazy('listamuni')
    def handle_no_permission(self):
        return HttpResponseRedirect(reverse_lazy('listamuni'))
    
class detalle(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    permission_required = 'home.view_municipio'
    model = Municipio
    template_name = 'municipio/detalle.html'
    def handle_no_permission(self):
        return HttpResponseRedirect(reverse_lazy('listamuni'))

class actualizar(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    permission_required = 'home.change_municipio'
    model = Municipio
    template_name = 'municipio/actualizar.html'
    fields = ['nombre', 'departamento']
    success_url = reverse_lazy('listamuni')
    def handle_no_permission(self):
        return HttpResponseRedirect(reverse_lazy('listamuni'))

class eliminar(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    permission_required = 'home.delete_municipio'
    model = Municipio
    template_name = 'municipio/eliminar.html'
    success_url = reverse_lazy('listamuni')
    def handle_no_permission(self):
        return HttpResponseRedirect(reverse_lazy('listamuni'))
