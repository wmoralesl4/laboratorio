from django.views.generic import ListView, CreateView, UpdateView, DeleteView, DetailView
from django.urls import reverse_lazy
from .models import Pais
from django.shortcuts import render
from django.db.models import Q
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login as auth_login
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView


class HomeTemplateView(LoginRequiredMixin, TemplateView):
    template_name = 'home.html'

class RegisterView(CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('listapais')
    template_name = 'registro.html'

    def form_valid(self, form):
        response = super().form_valid(form)
        auth_login(self.request, self.object)
        return response

class lista(ListView):
    model = Pais
    template_name = 'pais/listapais.html'
    context_object_name = 'paises'
    paginate_by = 10  
    def get_queryset(self):
        query = self.request.GET.get('pais')
        if query:
            return Pais.objects.filter(Q(nombre__icontains=query))
        else:
            return Pais.objects.all()
class detalle(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    permission_required = 'home.view_pais'
    model = Pais
    template_name = 'pais/detalle.html'
    def handle_no_permission(self):
        return HttpResponseRedirect(reverse_lazy('listapais'))
 
class crear(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    permission_required = 'home.add_pais'
    model = Pais
    template_name = 'pais/crear.html'
    fields = ['nombre']
    success_url = reverse_lazy('listapais')
    def handle_no_permission(self):
        return HttpResponseRedirect(reverse_lazy('listapais'))

class actualizar(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    permission_required = 'home.change_pais'
    model = Pais
    template_name = 'pais/actualizar.html'
    fields = ['nombre']
    success_url = reverse_lazy('listapais')
    def handle_no_permission(self):
        return HttpResponseRedirect(reverse_lazy('listapais'))

class eliminar(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    permission_required = 'home.delete_pais'
    model = Pais
    template_name = 'pais/eliminar.html'
    success_url = reverse_lazy('listapais')
    def handle_no_permission(self):
        return HttpResponseRedirect(reverse_lazy('listapais'))