
from django.urls import path, include
from . import viewDep, viewsPais, viewMunicipio
from django.contrib.auth.views import LoginView, LogoutView

urlpatterns = [
    #paises
    path('', viewsPais.HomeTemplateView.as_view(), name='home'),
    path('pais/', viewsPais.lista.as_view(), name='listapais'),
    path('pais/<int:pk>/', viewsPais.detalle.as_view(), name='detallepais'),
    path('pais/crear/', viewsPais.crear.as_view(), name='crearpais'),
    path('pais/<int:pk>/actualizar/', viewsPais.actualizar.as_view(), name='actualizarpais'),
    path('pais/<int:pk>/eliminar/', viewsPais.eliminar.as_view(), name='eliminarpais'),
    
    path('departamento/', viewDep.lista.as_view(), name='listadepto'),
    path('departamento/<int:pk>/', viewDep.detalle.as_view(), name='detalledepto'),
    path('departamento/crear/', viewDep.crear.as_view(), name='creardepto'),
    path('departamento/<int:pk>/actualizar/', viewDep.actualizar.as_view(), name='actualizardepto'),
    path('departamento/<int:pk>/eliminar/', viewDep.eliminar.as_view(), name='eliminardepto'),
    
    path('municipio/', viewMunicipio.lista.as_view(), name='listamuni'),
    path('municipio/<int:pk>/', viewMunicipio.detalle.as_view(), name='detallemuni'),
    path('municipio/crear/', viewMunicipio.crear.as_view(), name='crearmuni'),
    path('municipio/<int:pk>/actualizar/', viewMunicipio.actualizar.as_view(), name='actualizarmuni'),
    path('municipio/<int:pk>/eliminar/', viewMunicipio.eliminar.as_view(), name='eliminarmuni'),
    
    path('login/', LoginView.as_view(template_name='login.html'), name='login'),
    path('logout/', LogoutView.as_view(next_page='listapais'), name='logout'),
    path('register/', viewsPais.RegisterView.as_view(), name='register'),

    path('api/', include('home.urlsAPI')),

    
]
