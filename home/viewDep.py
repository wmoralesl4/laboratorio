from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from .models import Municipio, Pais, Departamento
from django.shortcuts import render
from django.db.models import Q
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login as auth_login
from django.http import HttpResponseRedirect


class lista(ListView):
    model = Departamento
    template_name = 'departamento/lista.html'
    context_object_name = 'departamentos'
    paginate_by = 10  
    def get_queryset(self):
        query = self.request.GET.get('departamento')
        if query:
            return Departamento.objects.filter(Q(nombre__icontains=query)) 
        else:
            return Departamento.objects.all()
        
class detalle(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    permission_required = 'catalogo.view_departamento'
    model = Departamento
    template_name = 'departamento/detalle.html'
    def handle_no_permission(self):
        return HttpResponseRedirect(reverse_lazy('listadepto'))

class crear(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    permission_required = 'catalogo.add_departamento'
    model = Departamento
    template_name = 'departamento/crear.html'
    fields = ['nombre', 'pais']
    success_url = reverse_lazy('listadepto')
    def handle_no_permission(self):
        return HttpResponseRedirect(reverse_lazy('listadepto'))

class actualizar(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    permission_required = 'catalogo.change_departamento'
    model = Departamento
    template_name = 'departamento/actualizar.html'
    fields = ['nombre', 'pais']
    success_url = reverse_lazy('listadepto')
    def handle_no_permission(self):
        return HttpResponseRedirect(reverse_lazy('listadepto'))

class eliminar(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    permission_required = 'catalogo.delete_departamento'
    model = Departamento
    template_name = 'departamento/eliminar.html'
    success_url = reverse_lazy('listadepto')
    def handle_no_permission(self):
        return HttpResponseRedirect(reverse_lazy('listadepto'))
    